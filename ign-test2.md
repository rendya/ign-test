**Tugas 2**
Duplikasi halaman [ini](https://modelunitednation.org/online/) menggunakan framework php. Untk assets ada [disini](https://drive.google.com/drive/folders/1gBcg01Vp0m93a2k77hdkP88ZQlcsLEzl?usp=sharing)


### Peraturan Wajib

- Anda **Diharuskan** menggunakan framework PHP (Codeigniter/Laravel)
- Anda diharuskan menggunakan repository (git) dalam mengerjakan tugas ini dan jangan lupa untuk menyetel git proyek anda menjadi **public**


### Apa yang akan kami nilai ?

- **Kesesuaian** pada peraturan diatas, berjalannya program tersebut dengan benar dan sesuai harapan
- Desain dan struktur code anda
- Menggunakan konsep **DRY** ataupun *clean code* dalam pengerjaan
- Git commit


### Pengumpulan pengerjaan

- Pengerjaan di push pada **repositori masing-masing atau di simpan dalam server milik anda.
- Jika disimpan dalam server, buat url dengan format **www.nama_anda.domain_anda**


Kirim link repository git atau url(jika disimpan dalam Server) anda ke https://wa.me/628128728797 dan jangan lupa untuk membuat repository anda menjadi **public** agar dapat diakses dan di *clone*.


Semangat dan Selamat Mengerjakan !! 

**IGN, Be the One, Be the Best, GO! GO! Champion!**