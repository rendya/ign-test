Anda adalah seorang *engineer* di International Global Network. Dalam satu event, IGN dapat menerima ribuan data pendaftar.

Pada suatu hari, divisi CRM dalam IGN meminta anda beserta team, membuat sebuah sistem dashboard yang dapat menampilkan data-data dari pendaftar dan pada akhirnya data tersebut dapat diolah menjadi sebuah informasi.

Anda diminta membuat suatu service yang dapat melakukan *request* pada API yang tersedia, menerima *response* dari API yang dituju, dan menyimpan data kedalam database yang anda telah siapkan.

**Tugas Utama anda Adalah**

- Mengirim *request* ke API yang telah disediakan  
- Menerima *response* dari API yang dituju dan menyimpannya kedalam *database* yang telah anda sediakan
- Jika data *response* telah tersedia di *database*, maka sistem anda tidak perlu memasukkan ulang data sehingga tidak ada duplikasi pada *database*
    
Anda dapat mempelajari dokumentasi API yang akan dituju [disini](https://gitlab.com/rendya/ign-test-api/-/blob/master/ign-api-docs.md)

## Informasi pengerjaan

### Peraturan Wajib

- Anda diharuskan menggunakan *native PHP* dalam mengerjakan tugas ini
- Anda **Tidak Diperbolehkan** menggunakan framework ataupun **External Library**
- Anda diharuskan menyimpan data yang diterima dari response kedalam *database*
- Aplikasi anda dapat meng-*update* informasi tanpa adanya duplikasi data
- Anda diharuskan menggunakan repository (git) dalam mengerjakan tugas ini dan jangan lupa untuk menyetel git proyek anda menjadi **public**

### Apa yang akan kami nilai ?

- **Kesesuaian** pada peraturan diatas, berjalannya program tersebut dengan benar dan sesuai harapan
- Menggunakan konsep **OOP** *(Object Oriented Programming)* dalam pengerjaan
- Desain dan struktur dari sistem anda
- Menggunakan konsep **DRY** ataupun *clean code* dalam pengerjaan
- Git commit

### Pengumpulan pengerjaan

- Buat file `README.md` yang menjelaskan tentang sistem anda dan cara menjalankannya. Dalam menjalankan sistem, harusnya dijalankan dengan perintah sederhana seperti contoh menjalankan perintah `php getApplicant.php` atau perintah sederhana lainnya untuk melakukan *request* ke *API* dan menyimpan data yang diterima kedalam database.
- Menyediakan satu file **konfigurasi** yang menyimpan informasi tentang database beserta kredensial lainnya. Jika project tersebut dijalankan secara lokal, maka hanya satu file tersebut yang diubah (untuk koneksi database dan lain-lain)
- Menyediakan satu file **migrasi** database yang dapat dijalankan dengan perintah sederhana seperti `php migration.php` (atau perintah yang lain). Sehingga ketika file tersebut dijalankan, secara otomatis membuat tabel dalam database beserta informasi yang diperlukan.
- Pengerjaan **wajib** di push pada **repositori masing-masing**

Kirim link repository git anda ke https://wa.me/6281287828797 dan jangan lupa untuk membuat repository anda menjadi **public** agar dapat diakses dan di *clone*.

Semangat dan Selamat Mengerjakan !! 

**IGN, Be the One, Be the Best, GO! GO! Champion!**