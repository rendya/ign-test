# International Global Network API Documentation

## Authentication

Dalam API ini tidak menggunakan autentikasi apapun.

## General

*Basic url untuk semua request adalah:*
`http://188.166.177.87/ignapi/`

## Get Applicants Data

### Request

```http

Content-Type: application/json
Authorization: none
```

### Response

```json
Status 200
Content-Type: application/json

[
  {
    "nama": "Diego Farminggo",
    "email": "diegofar@gmail.com",
    "birth_date": "1998-02-20",
    "country": "Mexico",
    "phone": "+526112611122",
    "registration_date": "2019-09-21",
    "need_pickup": true
  },
  {
    "nama": "Neil Azhar",
    "email": "neil@gmail.com",
    "birth_date": "1997-04-12",
    "country": "Pakistan",
    "phone": "+516112611122",
    "registration_date": "2019-09-22",
    "need_pickup": true
  },
  {
    "nama": "Budi Hasibuan",
    "email": "budihas@gmail.com",
    "birth_date": "1995-04-02",
    "country": "Indonesia",
    "phone": "+626112611122",
    "registration_date": "2019-09-22",
    "need_pickup": true
  }
]
```